let firstNumber = 12;
let secondNumber = 5;
let result = 0;

// [ARITHMETIC OPERATORS]
// Addition Operator - responsible for the addition two or more numbers
result = firstNumber + secondNumber;
console.log('Result of addition operator is ' + result);

// Subtraction Operator - responsible for the subtraction two or more numbers
result = firstNumber - secondNumber;
console.log('Result of subtraction operator is ' + result);

// Multiplication Operator - responsible for the multiplication two or more numbers
result = firstNumber * secondNumber;
console.log('Result of multiplication operator is ' + result);

// Division Operator - responsible for the division two or more numbers
result = firstNumber / secondNumber;
console.log('Result of division operator is ' + result);

// Modulo Operator - responsible for getting the remainder of two or more numbers
result = firstNumber % secondNumber;
console.log('Result of modulo operator is ' + result);

// [ASSIGNMENT OPERATORS]
// Reassignment Operator - should be a single equal sign, signifies reassignment or new value into existing variable
total = 27;
console.log('Result of re-assignment operation is ' + total);

// Addition Assignment Operator - uses the current value of the variable and adds a number to itself. Afterwards, reassigns it as the new value
total += 3;
// total = total + 3;
console.log('Result of addition assignment operation is ' + total);

// Subtraction Assignment Operator - uses the current value of the variable and subtracts a number to itself. Afterwards, reassigns it as the new value
total -= 5;
// total = total - 5;
console.log('Result of subtraction assignment operation is ' + total);

// Multiplication Assignment Operator - uses the current value of the variable and multiplies a number to itself. Afterwards, reassigns it as the new value
total *= 4;
// total = total * 4;
console.log('Result of multiplication assignment operation is ' + total);

// Division Assignment Operator - uses the current value of the variable and divides a number to itself. Afterwards, reassigns it as the new value
total /= 20;
// total = total / 4;
console.log('Result of division assignment operation is ' + total);

// [MULTIPLE OPERATORS]
let mdasTotal = 0;
let pemdasTotal = 0

// When doing multiple operations, the program follows the PEMDAS rule
mdasTotal = 2 + 1 - 5 * 4 / 1;
console.log(mdasTotal);

pemdasTotal = 5 ** 2 + (10 - 2) / 2 * 3;
console.log(pemdasTotal);

// [INCREMENT AND DECREMENT OPERATORS]
let incrementNumber = 1;
let decrementNumber = 5;

// Pre Increment
console.log(++incrementNumber);

// Pre Decrement
console.log(--decrementNumber);

// Post Increment
console.log(incrementNumber++);
console.log(incrementNumber);

// Post Decrement
console.log(decrementNumber--);
console.log(decrementNumber);

// [COERCION]
// Coercion - add a string to a non-string
let a = '10';
let b = 10;

console.log(a + b);

// Non-coercion
a = 10;
b = 10;

console.log(a + b);

// typeof is a keyword that will return the type of a variable
let stringType = 'hello';
let numberType = 1;
let booleanType = true;
let arrayType = [null, 0];
let objectType = {
	isNull: null,
	objectKey: 'value'
};

console.log(typeof stringType);
console.log(typeof numberType);
console.log(typeof booleanType);
console.log(typeof arrayType);
console.log(typeof objectType);

// boolean variables are represented as binary numbers
console.log(true + 1);
console.log(false + 1);

// [COMAPRISON OPERATORS]
// Equality Operator - equal value
console.log(5 == 5);
console.log('hello' == 'hello');
console.log(2 == '2');

// Strict Equality Operator - equal value and equal type
console.log(2 === '2');
console.log(true === 1);

// Iequality Operator - not equal value
console.log(5 != 5);
console.log('hello' != 'hello');
console.log(2 != '2');

// Strict Inequality Operator - not equal value and not equal type
console.log(2 !== '2');
console.log(true !== 1);

// [RELATIONAL OPERATORS]
let firstVariable = 5;
let secondVariable = '5';

console.log(firstVariable > secondVariable);
console.log(firstVariable < secondVariable);
console.log(firstVariable >= secondVariable);
console.log(firstVariable <= secondVariable);

// [LOGICAL OPERATORS]
let isLegalAge = true;
let isRegistered = false;

// AND Operator - returns true if both statements are true
console.log(isLegalAge && isRegistered);

// OR Operator - returns true if at least one statement is true
console.log(isLegalAge || isRegistered);

// NOT Operator - reverses the boolean value
console.log(!isLegalAge);


// Truthy and Falsey values
// Everything that is either empty, zero, null or undefined will equate to false, and everything that has some sort of value will equate to true
console.log([] == false);
console.log('' == false);
console.log(0 == false);
// console.log(null = false);
// console.log(undefined = false);